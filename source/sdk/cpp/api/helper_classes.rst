Helper Classes
============================

These are less-important helper classes to the main classes in the SDK.

.. doxygenclass:: Background
   :members:

.. doxygenclass:: CartesianCoordinateSystem
   :members:

.. doxygenclass:: CartesianScale
   :members:

.. doxygenclass:: ColumnStringIO
   :members:

.. doxygenclass:: ErrorBar
   :members:

.. doxygenclass:: Line
   :members:

.. doxygenclass:: PlotArea
   :members:

.. doxygenclass:: Range
   :members:

.. doxygenclass:: StatisticsSpreadsheet
   :members:

.. doxygenclass:: Symbol
   :members:

.. doxygenclass:: Value
   :members:

.. doxygenclass:: XmlStreamReader
   :members:
