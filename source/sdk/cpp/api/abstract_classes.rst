Abstract Classes
============================

These are abstract classes which provide interfaces for derived classes.

.. doxygenclass:: AbstractAspect
   :members:

.. doxygenclass:: AbstractColumn
   :members:

.. doxygenclass:: AbstractCoordinateSystem
   :members:

.. doxygenclass:: AbstractDataSource
   :members:

.. doxygenclass:: AbstractFileFilter
   :members:

.. doxygenclass:: AbstractPart
   :members:

.. doxygenclass:: AbstractPlot
   :members:

.. doxygenclass:: Plot
   :members:

.. doxygenclass:: WorksheetElement
   :members:

.. doxygenclass:: WorksheetElementContainer
   :members:

.. doxygenclass:: XYAnalysisCurve
   :members:
