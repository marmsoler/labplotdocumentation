Filters
===================

Filters handle importing and exporting data from/to various file formats.

.. doxygenclass:: AsciiFilter
   :members:

.. doxygenclass:: BinaryFilter
   :members:

.. doxygenclass:: CANFilter
   :members:

.. doxygenclass:: FITSFilter
   :members:

.. doxygenclass:: HDF5Filter
   :members:

.. doxygenclass:: ImageFilter
   :members:

.. doxygenclass:: JsonFilter
   :members:

.. doxygenclass:: MatioFilter
   :members:

.. doxygenclass:: McapFilter
   :members:

.. doxygenclass:: NetCDFFilter
   :members:

.. doxygenclass:: OdsFilter
   :members:

.. doxygenclass:: ROOTFilter
   :members:

.. doxygenclass:: ReadStatFilter
   :members:

.. doxygenclass:: SpiceFilter
   :members:

.. doxygenclass:: VectorBLFFilter
   :members:

.. doxygenclass:: XLSXFilter
   :members:
