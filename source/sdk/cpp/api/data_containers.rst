Data Containers
===================

Data Containers are groups of columns with additional functionalities.

.. doxygenclass:: Matrix
   :members:

.. doxygenclass:: Spreadsheet
   :members:
