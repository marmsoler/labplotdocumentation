Worksheet Element Containers
============================

This is a container for worksheet elements and has a plot area to hold plot area elements.

.. doxygenclass:: CartesianPlot
   :members:
