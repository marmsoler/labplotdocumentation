Plot Area Elements
===================

These are non-plot objects that can be placed within a plot area.

.. doxygenclass:: Axis
   :members:

.. doxygenclass:: CartesianPlotLegend
   :members:

.. doxygenclass:: CustomPoint
   :members:

.. doxygenclass:: ReferenceLine
   :members:

.. doxygenclass:: ReferenceRange
   :members:
