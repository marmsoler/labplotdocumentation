Worksheet Elements
===================

These are objects that can be drawn on a worksheet or a worksheet element container.

.. doxygenclass:: Image
   :members:

.. doxygenclass:: InfoElement
   :members:

.. doxygenclass:: TextLabel
   :members:
