Column
===================

Column is the fundamental data source in LabPlot.

.. doxygenclass:: Column
   :members:
