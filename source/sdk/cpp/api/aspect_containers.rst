Aspect Containers
===================

Aspect Containers are use to organize aspects.

.. doxygenclass:: Folder
   :members:

.. doxygenclass:: Project
   :members:
