Plots
===================

These are the plots that can be drawn within a plot area.

.. doxygenclass:: BarPlot
   :members:

.. doxygenclass:: BoxPlot
   :members:

.. doxygenclass:: Histogram
   :members:

.. doxygenclass:: KDEPlot
   :members:

.. doxygenclass:: LollipopPlot
   :members:

.. doxygenclass:: ProcessBehaviorChart
   :members:

.. doxygenclass:: QQPlot
   :members:

.. doxygenclass:: RunChart
   :members:

.. doxygenclass:: XYConvolutionCurve
   :members:

.. doxygenclass:: XYCorrelationCurve
   :members:

.. doxygenclass:: XYCurve
   :members:

.. doxygenclass:: XYDataReductionCurve
   :members:

.. doxygenclass:: XYDifferentiationCurve
   :members:

.. doxygenclass:: XYEquationCurve
   :members:

.. doxygenclass:: XYFitCurve
   :members:

.. doxygenclass:: XYFourierFilterCurve
   :members:

.. doxygenclass:: XYFourierTransformCurve
   :members:

.. doxygenclass:: XYFunctionCurve
   :members:

.. doxygenclass:: XYHilbertTransformCurve
   :members:

.. doxygenclass:: XYIntegrationCurve
   :members:

.. doxygenclass:: XYInterpolationCurve
   :members:

.. doxygenclass:: XYSmoothCurve
   :members:
