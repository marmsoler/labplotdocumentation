.. _sdk_examples:

SDK Examples
===================

The following examples show various uses of the LabPlot SDK for inspiration.

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   :glob:
   
   examples/**/*
