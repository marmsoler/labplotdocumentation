.. _sdk_cpp:

C++ SDK
===================

The C++ SDK allows LabPlot's various functionalities to be embedded in C++ applications.

For instructions on how to set up the necessary components and utilize the library, please refer to the "Getting Started" section. The API documentation is located in the "API" section.

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   :glob:
   
   cpp/getting_started
   cpp/api
