.. meta::
   :description: Tutorials and HOWTOs
   :keywords: LabPlot, documentation, user manual, data analysis, data visualization, curve fitting, open source, free, help, learn, cursor, FITS, tools

.. metadata-placeholder

   :authors: - LabPlot Team

.. _tutorials:

Tutorials
=====================

This section contains tutorials and HOWTOs contributed by the developers and users of LabPlot that will help to become more familiar with LabPlot and to see the application in action. Please also check out `introduction videos on YouTube <https://www.youtube.com/watch?v=Ngf1g3S5C0A&list=PL_dfQNSnql9Ol0zRBnPU8kcTGUwqq4JkJ>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   tutorials/tutorials_live_data
